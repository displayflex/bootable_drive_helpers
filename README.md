# How to Create a Bootable Flash Drive in MacOS (Catalina)


- **CHOOSE THE CORRECT IMAGE FOR YOUR CPU ARCHITECTURE**: Generally `x64`.
- **FORMAT VIA DISK UTILITY**: Set the format to MS-DOS (FAT) and the scheme to GUID Partition Map
- Copy Image: Via `Etcher`, `dd` or similar.
- Restart & Select **EFI** boot**: Note on Macs, you can hold down the Opt key (on wired keyboards) throughout the restart process.

## Parrot OS

The steps below outline how to create a bootable drive with parrot OS, persistence and encryption.

### Copy image:

When downloading an image to boot from a mac it needs to be of type: `x64`. The steps below use `Parrot-kde-security-4.9.1_x64.iso`.

* Insert a Fat32, partition mapped formatted flash disk

Either use Etcher to write the image, or the `dd` commands below.

* `sudo diskutil unmountDisk /dev/disk2`
* `dd if=Parrot-security-4.6_amd64.iso of=/dev/rdisk2 bs=1m`

Which should take about 2 mins on fast disk  - **On OSX, it is important to use /dev/rdiskX instead of /dev/diskX as it provides raw (thus way faster) access to the device. On Linux, just pass /dev/sdX as you normally do.**

## Create Partitions

Create a new ext4 partition called persistence in GParted
May need the following wipefs step, needed to Get GParted to recognise the multiple partitions.

`wipefs -o 0x8001 -f /dev/sdb`
## Encrypt the persistence volume



```
sudo umount /dev/sdb3
sudo wipefs -a /dev/sdb3
sudo cryptsetup --verbose --verify-passphrase luksFormat /dev/sdb3
sudo cryptsetup luksOpen /dev/sdb3 my_part
sudo mkfs.ext4 -L persistence /dev/mapper/my_part
sudo e2label /dev/mapper/my_part persistence
```

Then activate persistence:

```
sudo echo "/ union" > /media/USER/persistence/persistence.conf
```

Permission issues seem to mean you need vim for the last step - ensure the drive is mounted before running the command. 

## Referenceso

* [Etcher - 3rd party image writing software](https://www.balena.io/etcher/)
* [32 or 64 bit software](https://www.alesis.com/kb/article/1616)
* [Formatting a Fas Drive in OSX](https://ubuntu.com/tutorials/create-a-usb-stick-on-macos#3-prepare-the-usb-stick)
* [Create Parrot OS pendrive with encrypted persistence volume](https://www.rzegocki.pl/blog/create-parrot-os-pendrive-with-encrypted-persistence-volume/)
