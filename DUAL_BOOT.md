# Dual Booting

* Always resize partition within initial OS, then install using entire partition - saves manually creating partitions etc.
* Once installed, you'll still need to add a grub entry to be able to boot into the original OS. Be sure to also add an entry to boot from USB.
* In the interim you can access the original OS by pressing c then typing exit in GRUB.